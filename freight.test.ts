import { exportedForTesting } from './freight';
const { getWorldDMs } = exportedForTesting;

test('getWorldDMs() should return no DMs', () => {
    let world = {
        UWP: {
            Starport: 12,
            Size: 5,
            Atmosphere: 5,
            Hydrographics: 5,
            Population: 5,
            Government: 5,
            LawLevel: 5,
            TechLevel: 7
        },
        Zone: 'None',
        TradeCodes: ['Ag', 'Ga', 'Lt', 'Ni']
    };

    expect(getWorldDMs(world)).toEqual(0);
});

test('getWorldDMs() should return correct population DMs', () => {
    let world = {
        UWP: {
            Starport: 12,
            Size: 5,
            Atmosphere: 5,
            Hydrographics: 5,
            Population: 1,
            Government: 5,
            LawLevel: 5,
            TechLevel: 7
        },
        Zone: 'None',
        TradeCodes: ['Ag', 'Ga', 'Lt', 'Ni']
    };

    expect(getWorldDMs(world)).toEqual(-4);

    world.UWP.Population = 6;

    expect(getWorldDMs(world)).toEqual(2);

    world.UWP.Population = 8;

    expect(getWorldDMs(world)).toEqual(4);
});

test('getWorldDMs() should return correct zoneDMs', () => {
    let world = {
        UWP: {
            Starport: 12,
            Size: 5,
            Atmosphere: 5,
            Hydrographics: 5,
            Population: 5,
            Government: 5,
            LawLevel: 5,
            TechLevel: 7
        },
        Zone: 'Amber',
        TradeCodes: ['Ag', 'Ga', 'Lt', 'Ni']
    };

    expect(getWorldDMs(world)).toEqual(-2);

    world.Zone = 'Red';

    expect(getWorldDMs(world)).toEqual(-6);
});

test('getWorldDMs() should return correct starportDMs', () => {
    let world = {
        UWP: {
            Starport: 10,
            Size: 5,
            Atmosphere: 5,
            Hydrographics: 5,
            Population: 5,
            Government: 5,
            LawLevel: 5,
            TechLevel: 7
        },
        Zone: 'None',
        TradeCodes: ['Ag', 'Ga', 'Lt', 'Ni']
    };

    expect(getWorldDMs(world)).toEqual(2);

    world.UWP.Starport = 11;

    expect(getWorldDMs(world)).toEqual(1);

    world.UWP.Starport = 14;

    expect(getWorldDMs(world)).toEqual(-1);

    world.UWP.Starport = 31;

    expect(getWorldDMs(world)).toEqual(-3);
});

test('getWorldDMs() should return correct techDMs', () => {
    let world = {
        UWP: {
            Starport: 12,
            Size: 5,
            Atmosphere: 5,
            Hydrographics: 5,
            Population: 5,
            Government: 5,
            LawLevel: 5,
            TechLevel: 6
        },
        Zone: 'None',
        TradeCodes: ['Ag', 'Ga', 'Lt', 'Ni']
    };

    expect(getWorldDMs(world)).toEqual(-1);

    world.UWP.TechLevel = 9;

    expect(getWorldDMs(world)).toEqual(2);
});