import { getTradeCodes, getUWP, getWorld } from "./worldinfo"


test('getUWP() returns a valid UWP', () => {
    expect(getUWP('A123456-7')).toEqual({
        Starport: 10,
        Size: 1,
        Atmosphere: 2,
        Hydrographics: 3,
        Population: 4,
        Government: 5,
        LawLevel: 6,
        TechLevel: 7
    });

    expect(getUWP('X9876543')).toEqual({
        Starport: 31,
        Size: 9,
        Atmosphere: 8,
        Hydrographics: 7,
        Population: 6,
        Government: 5,
        LawLevel: 4,
        TechLevel: 3
    });
});

test('getTradeCodes() should return proper trade codes', () => {
    let uwp = getUWP('A555556-7')
    expect(getTradeCodes(uwp)).toContain('Ag');

    uwp = getUWP('X000556-7')
    expect(getTradeCodes(uwp)).toContain('As');

    uwp = getUWP('X555000-7')
    expect(getTradeCodes(uwp)).toContain('Ba');

    uwp = getUWP('X530555-7')
    expect(getTradeCodes(uwp)).toContain('De');

    uwp = getUWP('X5B5555-7')
    expect(getTradeCodes(uwp)).toContain('Fl');

    uwp = getUWP('C555555-7')
    expect(getTradeCodes(uwp)).toContain('Ga');

    uwp = getUWP('C555A55-7')
    expect(getTradeCodes(uwp)).toContain('Hi');

    uwp = getUWP('A555555-F')
    expect(getTradeCodes(uwp)).toContain('Ht');

    uwp = getUWP('C515555-7')
    expect(getTradeCodes(uwp)).toContain('Ic');

    uwp = getUWP('C575B55-7')
    expect(getTradeCodes(uwp)).toContain('In');

    uwp = getUWP('C555155-7')
    expect(getTradeCodes(uwp)).toContain('Lo');

    uwp = getUWP('C555555-3')
    expect(getTradeCodes(uwp)).toContain('Lt');

    uwp = getUWP('C521755-7')
    expect(getTradeCodes(uwp)).toContain('Na');

    uwp = getUWP('C555555-7')
    expect(getTradeCodes(uwp)).toContain('Ni');

    uwp = getUWP('C543555-7')
    expect(getTradeCodes(uwp)).toContain('Po');

    uwp = getUWP('C565755-7')
    expect(getTradeCodes(uwp)).toContain('Ri');

    uwp = getUWP('C55A555-7')
    expect(getTradeCodes(uwp)).toContain('Wa');

    uwp = getUWP('C505555-7')
    expect(getTradeCodes(uwp)).toContain('Va');
});

test('getWorld() should return a world', () => {
    expect(getWorld('C555555-5')).toEqual({
        UWP: {
            Starport: 12,
            Size: 5,
            Atmosphere: 5,
            Hydrographics: 5,
            Population: 5,
            Government: 5,
            LawLevel: 5,
            TechLevel: 5
        },
        Zone: 'None',
        TradeCodes: ['Ag', 'Ga', 'Lt', 'Ni']
    });

    expect(getWorld('C555555-5', 'Amber')).toEqual({
        UWP: {
            Starport: 12,
            Size: 5,
            Atmosphere: 5,
            Hydrographics: 5,
            Population: 5,
            Government: 5,
            LawLevel: 5,
            TechLevel: 5
        },
        Zone: 'Amber',
        TradeCodes: ['Ag', 'Ga', 'Lt', 'Ni', 'Az']
    });

    expect(getWorld('C555555-5', 'Red')).toEqual({
        UWP: {
            Starport: 12,
            Size: 5,
            Atmosphere: 5,
            Hydrographics: 5,
            Population: 5,
            Government: 5,
            LawLevel: 5,
            TechLevel: 5
        },
        Zone: 'Red',
        TradeCodes: ['Ag', 'Ga', 'Lt', 'Ni', 'Rz']
    });
});