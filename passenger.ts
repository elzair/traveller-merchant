import { constrain, dice, r2d6, sample } from './helpers';
import { World } from './worldinfo';

export interface PassengerInfo {
    number: number,
    exciting: Array<string>
}

const PassengerTraffic: Array<number> = [
    0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6, 7, 8, 9, 10
];

const RandomPassenger: Array<string> = [
    "Refugee - Political",
    "Refugee - Economic",
    "Starting a New Life Offworld",
    "Mercenary",
    "Spy",
    "Corporate Executive",
    "Out to See the Universe",
    "Tourist",
    "Wide-Eyed Yokel",
    "Adventurer",
    "Explorer",
    "Claustrophobic",
    "Expectant Mother",
    "Wants to Stowaway or Join the Crew",
    "Possesses Something Dangerous or Illegal",
    "Causes Trouble",
    "Unusually Pretty or Handsome",
    "Engineer",
    "Ex-Scout",
    "Wanderer",
    "Thief or Other Criminal",
    "Scientist",
    "Journalist or Researcher",
    "Entertainer",
    "Gamber",
    "Rich Noble - Complains a Lot",
    "Rich Noble - Eccentric",
    "Rich Noble - Raconteur",
    "Diplomat on a Mission",
    "Agent on a Mission",
    "Patron",
    "Alien",
    "Bounty Hunter",
    "On the Run",
    "Wants to be on Board the Travellers' Ship for Some Reason",
    "Hijacker or Pirate Agent",
];

export function seekPassengers(
    checkEffect: number,
    maxStewardSkill: number,
    source: World,
    destination: World,
    distance: number, 
    passenger: string,
    userDM: number = 0,
    excitingPct: number = 0.5,
    passengerPct: number = 0.2
): PassengerInfo {
    let passengerDM: number = 0;
    if (passenger === "High") {
        passengerDM = -4;
    } else if (passenger === "Low") {
        passengerDM = 1;
    }

    const sourceDMs: number = getWorldDMs(source);
    const destDMs: number = getWorldDMs(destination);
    
    const distanceDM: number = Math.min(0, (distance-1)*-1);

    const rollDMS: number = checkEffect + maxStewardSkill + passengerDM
        + sourceDMs + destDMs  + distanceDM + userDM;
    const roll: number = constrain(r2d6() + rollDMS, 0, PassengerTraffic.length-1);
    const val = PassengerTraffic[roll];
    const numPassengers = dice(val, 6);

    return {
        number: numPassengers,
        exciting: getExcitingPassengers(
            numPassengers, 
            excitingPct, 
            passengerPct
        )
    };
}

function getExcitingPassengers(
    numPassengers: number,
    excitingPct: number = 0.5,
    passengerPct: number = 0.2
): Array<string> {
    const excitingChance: number = Math.random();
    // let excitingPassengers: Array<string> = [];

    if (excitingChance <= excitingPct) {
        const numExcitingPassengers: number = 
            Math.round(numPassengers * passengerPct);
        // for (let i=0; i< numExcitingPassengers; i++) {
        //     excitingPassengers.push(sample(RandomPassenger));
        // }
        return [ ...Array(numExcitingPassengers).keys() ]
            .map( i => sample(RandomPassenger));
    }

    // return excitingPassengers;
    return [];
}

function getWorldDMs(world: World): number {
    let popDM: number = 0;
    if (world.UWP.Population <= 1) {
        popDM = -4;
    } else if (world.UWP.Population === 6 || world.UWP.Population === 7) {
        popDM = 1;
    } else if (world.UWP.Population >= 8) {
        popDM = 3;
    }

    let zoneDM: number = 0;
    if (world.Zone === "Amber") {
        zoneDM = 1;
    } else if (world.Zone === "Red") {
        zoneDM = -4;
    }

    let starportDM: number = 0;
    if (world.UWP.Starport === 10 ) {
        starportDM = 2;
    } else if (world.UWP.Starport === 11) {
        starportDM = 1;
    } else if (world.UWP.Starport === 14) {
        starportDM = -1;
    } else if (world.UWP.Starport === 31) {
        starportDM = -3;
    
    }
    
    return popDM + zoneDM + starportDM;
}

export const exportedForTesting = {
    getWorldDMs
}