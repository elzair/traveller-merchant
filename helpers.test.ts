import { constrain, die, tetra2num, zip } from './helpers';

test('die() should throw error when passed negative number', () => {
    try {
        die(-1)
    } catch (ex) {
        expect(ex.message).toEqual('die() input is negative -1');
    }
});

test('tetra2num() should produce correct output', () => {
    expect(tetra2num('0')).toEqual(0);
    expect(tetra2num('1')).toEqual(1);
    expect(tetra2num('2')).toEqual(2);
    expect(tetra2num('3')).toEqual(3);
    expect(tetra2num('4')).toEqual(4);
    expect(tetra2num('5')).toEqual(5);
    expect(tetra2num('6')).toEqual(6);
    expect(tetra2num('7')).toEqual(7);
    expect(tetra2num('8')).toEqual(8);
    expect(tetra2num('9')).toEqual(9);
    expect(tetra2num('A')).toEqual(10);
    expect(tetra2num('B')).toEqual(11);
    expect(tetra2num('C')).toEqual(12);
    expect(tetra2num('D')).toEqual(13);
    expect(tetra2num('E')).toEqual(14);
    expect(tetra2num('F')).toEqual(15);
    expect(tetra2num('G')).toEqual(16);
    expect(tetra2num('H')).toEqual(17);
    expect(tetra2num('J')).toEqual(18);
    expect(tetra2num('K')).toEqual(19);
    expect(tetra2num('L')).toEqual(20);
    expect(tetra2num('M')).toEqual(21);
    expect(tetra2num('N')).toEqual(22);
    expect(tetra2num('P')).toEqual(23);
    expect(tetra2num('Q')).toEqual(24);
    expect(tetra2num('R')).toEqual(25);
    expect(tetra2num('S')).toEqual(26);
    expect(tetra2num('T')).toEqual(27);
    expect(tetra2num('U')).toEqual(28);
    expect(tetra2num('V')).toEqual(29);
    expect(tetra2num('W')).toEqual(30);
    expect(tetra2num('X')).toEqual(31);
    expect(tetra2num('Y')).toEqual(32);
    expect(tetra2num('Z')).toEqual(33);
    expect(tetra2num('a')).toEqual(10);
    expect(tetra2num('b')).toEqual(11);
    expect(tetra2num('c')).toEqual(12);
    expect(tetra2num('d')).toEqual(13);
    expect(tetra2num('e')).toEqual(14);
    expect(tetra2num('f')).toEqual(15);
    expect(tetra2num('g')).toEqual(16);
    expect(tetra2num('h')).toEqual(17);
    expect(tetra2num('j')).toEqual(18);
    expect(tetra2num('k')).toEqual(19);
    expect(tetra2num('l')).toEqual(20);
    expect(tetra2num('m')).toEqual(21);
    expect(tetra2num('n')).toEqual(22);
    expect(tetra2num('p')).toEqual(23);
    expect(tetra2num('q')).toEqual(24);
    expect(tetra2num('r')).toEqual(25);
    expect(tetra2num('s')).toEqual(26);
    expect(tetra2num('t')).toEqual(27);
    expect(tetra2num('u')).toEqual(28);
    expect(tetra2num('v')).toEqual(29);
    expect(tetra2num('w')).toEqual(30);
    expect(tetra2num('x')).toEqual(31);
    expect(tetra2num('y')).toEqual(32);
    expect(tetra2num('z')).toEqual(33);
});

test('tetra2num() should throw error on invalid input', () => {
    try {
        tetra2num('I');
    } catch (ex) {
        expect(ex.message).toEqual('tetra2num() passed invalid character: I');
    }

    try {
        tetra2num('O');
    } catch (ex) {
        expect(ex.message).toEqual('tetra2num() passed invalid character: O');
    }

    try {
        tetra2num('i');
    } catch (ex) {
        expect(ex.message).toEqual('tetra2num() passed invalid character: i');
    }

    try {
        tetra2num('o');
    } catch (ex) {
        expect(ex.message).toEqual('tetra2num() passed invalid character: o');
    }

    try {
        tetra2num(' ');
    } catch (ex) {
        expect(ex.message).toEqual('tetra2num() passed invalid character:  ');
    }
})

test('constrain() should constrain input', () => {
    expect(constrain(-5, -1, 3)).toEqual(-1);
    expect(constrain(5, -1, 3)).toEqual(3);
    expect(constrain(1, -1, 3)).toEqual(1);
});

test('zip() should zip arrays of same length', () => {
    const arr1 = [5, 6, 7, 8];
    const arr2 = ['Dog', 'Cat', 'Mouse', 'Squirrel'];

    expect(zip(arr1, arr2)).toEqual([
        [5, 'Dog'],
        [6, 'Cat'],
        [7, 'Mouse'],
        [8, 'Squirrel']
    ]);
});

test('zip() should throw error when passed 2 arrays of different lengths', () => {
    const arr1 = [5, 6, 7, 8, 9];
    const arr2 = ['Dog', 'Cat', 'Mouse', 'Squirrel'];

    try {
        zip(arr1, arr2);
    } catch (ex) {
        expect(ex.message).toEqual('Arrays must have the same length');
    }
})