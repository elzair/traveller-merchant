

export const PassageAndFreight = {
    "High Passage": [9000, 14000, 21000, 34000, 60000, 210000],
    "Middle Passage": [6500, 10000, 14000, 23000, 40000, 130000],
    "Basic Passage": [2000, 3000, 5000, 8000, 14000, 55000],
    "Low Passage": [700, 1300, 2200, 3900, 7200, 27000],
    "Freight": [1000, 1600, 2600, 4400, 8500, 32000]
}

export interface TradeGood {
    GoodType: string,
    Availability: string | Array<string>,
    Tons: Array<number>,
    BasePrice: number,
    PurchaseDM: Record<string, number>,
    SaleDM: Record<string, number>,
    Illegal: boolean
}

export const TradeGoods: Array<TradeGood> = [
    {
        GoodType: "Common Electronics",
        Availability: "All",
        Tons: [2, 10],
        BasePrice: 20000,
        PurchaseDM: { "In": 2, "Ht": 3, "Ri": 1 },
        SaleDM: { "Ni": 2, "Lt": 1, "Po": 1 },
        Illegal: false
    }, {
        GoodType: "Common Industrial Goods",
        Availability: "All",
        Tons: [2, 10],
        BasePrice: 10000,
        PurchaseDM: { "Na": 2, "In": 5 },
        SaleDM: { "Ni": 3, "Ag": 2 },
        Illegal: false
    }, {
        GoodType: "Common Manufactured Goods",
        Availability: "All",
        Tons: [2, 10],
        BasePrice: 20000,
        PurchaseDM: { "Na": 2, "In": 5 },
        SaleDM: { "Ni": 3, "Hi": 2 },
        Illegal: false
    }, {
        GoodType: "Common Raw Materials",
        Availability: "All",
        Tons: [2, 20],
        BasePrice: 5000,
        PurchaseDM: { "Ag": 3, "Ga": 2 },
        SaleDM: { "In": 2, "Po": 2 },
        Illegal: false
    }, {
        GoodType: "Common Consumables",
        Availability: "All",
        Tons: [2, 20],
        BasePrice: 500,
        PurchaseDM: { "Ag": 3, "Wa": 2, "Ga": 1, "As": -4 },
        SaleDM: { "As": 1, "Fl": 1, "Ic": 1, "Hi": 1 },
        Illegal: false
    }, {
        GoodType: "Common Ore",
        Availability: "All",
        Tons: [2, 20],
        BasePrice: 1000,
        PurchaseDM: { "As": 4 },
        SaleDM: { "In": 3, "Ni": 1 },
        Illegal: false
    }, {
        GoodType: "Advanced Electronics",
        Availability: ["In", "Ht"],
        Tons: [1, 5],
        BasePrice: 100000,
        PurchaseDM: { "In": 2, "Ht": 3 },
        SaleDM: { "Ni": 1, "Ri": 2, "As": 3 },
        Illegal: false
    }, {
        GoodType: "Advanced Machine Parts",
        Availability: ["In", "Ht"],
        Tons: [1, 5],
        BasePrice: 75000,
        PurchaseDM: { "In": 2, "Ht": 1 },
        SaleDM: { "As": 2, "Ni": 1 },
        Illegal: false
    }, {
        GoodType: "Advanced Manufactured Goods",
        Availability: ["In", "Ht"],
        Tons: [1, 5],
        BasePrice: 100000,
        PurchaseDM: { "In": 1 },
        SaleDM: { "Hi": 1, "Ri": 2 },
        Illegal: false
    }, {
        GoodType: "Advanced Weapons",
        Availability: ["In", "Ht"],
        Tons: [1, 5],
        BasePrice: 150000,
        PurchaseDM: { "Ht": 2 },
        SaleDM: { "Po": 1, "Az": 2, "Rz": 4 },
        Illegal: false
    }, {
        GoodType: "Advanced Vehicles",
        Availability: ["In", "Ht"],
        Tons: [1, 5],
        BasePrice: 180000,
        PurchaseDM: { "Ht": 2 },
        SaleDM: { "As": 2, "Ri": 2 },
        Illegal: false
    }, {
        GoodType: "Biochemicals",
        Availability: ["Ag", "Wa"],
        Tons: [1, 5],
        BasePrice: 50000,
        PurchaseDM: { "Ag": 1, "Wa": 2 },
        SaleDM: { "In": 2 },
        Illegal: false
    }, {
        GoodType: "Crystals & Gems",
        Availability: ["As", "De", "Ic"],
        Tons: [1, 5],
        BasePrice: 20000,
        PurchaseDM: { "As": 2, "De": 1, "Ic": 1 },
        SaleDM: { "In": 3, "Ri": 2 },
        Illegal: false
    }, {
        GoodType: "Cybernetics",
        Availability: ["Ht"],
        Tons: [1, 1],
        BasePrice: 250000,
        PurchaseDM: { "Ht": 1 },
        SaleDM: { "As": 1, "Ic": 1, "Ri": 2 },
        Illegal: false
    }, {
        GoodType: "Live Animals",
        Availability: ["Ag", "Ga"],
        Tons: [1, 10],
        BasePrice: 10000,
        PurchaseDM: { "Ag": 2 },
        SaleDM: { "Lo": 3 },
        Illegal: false
    }, {
        GoodType: "Luxury Consumables",
        Availability: ["Ag", "Ga", "Wa"],
        Tons: [1, 10],
        BasePrice: 20000,
        PurchaseDM: { "Ag": 2, "Wa": 1 },
        SaleDM: { "Ri": 2, "Hi": 2 },
        Illegal: false
    }, {
        GoodType: "Luxury Goods",
        Availability: ["Hi"],
        Tons: [1, 1],
        BasePrice: 200000,
        PurchaseDM: { "Hi": 1 },
        SaleDM: { "Ri": 4 },
        Illegal: false
    }, {
        GoodType: "Medical Supplies",
        Availability: ["Ht", "Hi"],
        Tons: [1, 5],
        BasePrice: 50000,
        PurchaseDM: { "Ht": 2 },
        SaleDM: { "In": 2, "Po": 1, "Ri": 1 },
        Illegal: false
    }, {
        GoodType: "Petrochemicals",
        Availability: ["De", "Fl", "Ic", "Wa"],
        Tons: [1, 10],
        BasePrice: 10000,
        PurchaseDM: { "De": 2 },
        SaleDM: { "In": 2, "Ag": 1, "Lt": 2 },
        Illegal: false
    }, {
        GoodType: "Pharmaceuticals",
        Availability: ["As", "De", "Hi", "Wa"],
        Tons: [1, 1],
        BasePrice: 100000,
        PurchaseDM: { "As": 2, "Hi": 1 },
        SaleDM: { "Ri": 2, "Lt": 1 },
        Illegal: false
    }, {
        GoodType: "Polymers",
        Availability: ["In"],
        Tons: [1, 10],
        BasePrice: 7000,
        PurchaseDM: { "In": 1 },
        SaleDM: { "Ri": 2, "Ni": 1 },
        Illegal: false
    }, {
        GoodType: "Precious Metals",
        Availability: ["As", "De", "Ic", "Fl"],
        Tons: [1, 1],
        BasePrice: 50000,
        PurchaseDM: { "As": 3, "De": 1, "Ic": 2 },
        SaleDM: { "Ri": 3, "In": 2, "Ht": 1 },
        Illegal: false
    }, {
        GoodType: "Radioactives",
        Availability: ["As", "De", "Lo"],
        Tons: [1, 1],
        BasePrice: 1000000,
        PurchaseDM: { "As": 2, "Lo": 2 },
        SaleDM: { "In": 3, "Ht": 1, "Ni": -2, "Ag": -3 },
        Illegal: false
    }, {
        GoodType: "Robots",
        Availability: ["In"],
        Tons: [1, 5],
        BasePrice: 400000,
        PurchaseDM: { "In": 1 },
        SaleDM: { "Ag": 2, "Ht": 1 },
        Illegal: false
    }, {
        GoodType: "Spices",
        Availability: ["Ga", "De", "Wa"],
        Tons: [1, 10],
        BasePrice: 6000,
        PurchaseDM: { "De": 2 },
        SaleDM: { "Hi": 2, "Ri": 3, "Po": 3 },
        Illegal: false
    }, {
        GoodType: "Textiles",
        Availability: ["Ag", "Ni"],
        Tons: [1, 20],
        BasePrice: 3000,
        PurchaseDM: { "Ag": 7 },
        SaleDM: { "Hi": 3, "Na": 2 },
        Illegal: false
    }, {
        GoodType: "Uncommon Ore",
        Availability: ["As", "Ic"],
        Tons: [1, 20],
        BasePrice: 5000,
        PurchaseDM: { "As": 4 },
        SaleDM: { "In": 3, "Ni": 1 },
        Illegal: false
    }, {
        GoodType: "Uncommon Raw Materials",
        Availability: ["Ag", "De", "Wa"],
        Tons: [1, 10],
        BasePrice: 20000,
        PurchaseDM: { "Ag": 2, "Wa": 1 },
        SaleDM: { "In": 2, "Ht": 1 },
        Illegal: false
    }, {
        GoodType: "Wood",
        Availability: ["Ag", "Ga"],
        Tons: [1, 20],
        BasePrice: 1000,
        PurchaseDM: { "Ag": 6 },
        SaleDM: { "Ri": 2, "In": 1 },
        Illegal: false
    }, {
        GoodType: "Vehicles",
        Availability: ["In", "Ht"],
        Tons: [1, 10],
        BasePrice: 15000,
        PurchaseDM: { "In": 2, "Ht": 1 },
        SaleDM: { "Ni": 2, "Hi": 1 },
        Illegal: false
    }, {
        GoodType: "Illegal Biochemicals",
        Availability: ["Ag", "Wa"],
        Tons: [1, 5],
        BasePrice: 50000,
        PurchaseDM: { "Wa": 2 },
        SaleDM: { "In": 6 },
        Illegal: true
    }, {
        GoodType: "Cybernetics, Illegal",
        Availability: ["Ht"],
        Tons: [1, 1],
        BasePrice: 250000,
        PurchaseDM: { "Ht": 1 },
        SaleDM: { "As": 4, "Ic": 4, "Ri": 8, "Az": 6, "Rz": 6 },
        Illegal: true
    }, {
        GoodType: "Drugs, Illegal",
        Availability: ["As", "De", "Hi", "Wa"],
        Tons: [1, 1],
        BasePrice: 100000,
        PurchaseDM: { "As": 1, "De": 1, "Ga": 1, "Wa": 1 },
        SaleDM: { "Ri": 6, "Hi": 6 },
        Illegal: true
    }, {
        GoodType: "Luxuries, Illegal",
        Availability: ["Ag", "Ga", "Wa"],
        Tons: [1, 1],
        BasePrice: 50000,
        PurchaseDM: { "Ag": 2, "Wa": 1 },
        SaleDM: { "Ri": 6, "Hi": 4 },
        Illegal: true
    }, {
        GoodType: "Weapons, Illegal",
        Availability: ["In", "Ht"],
        Tons: [1, 5],
        BasePrice: 150000,
        PurchaseDM: { "Ht": 2 },
        SaleDM: { "Po": 6, "Az": 8, "Rz": 10 },
        Illegal: true
    }, {
        GoodType: "Exotics",
        Availability: "Special",
        Tons: [0, 1],
        BasePrice: 0,
        PurchaseDM: {},
        SaleDM: {},
        Illegal: false
    }
];