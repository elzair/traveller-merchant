import { dice, r1d6, r2d6 } from './helpers';
import { World } from './worldinfo';

const FreightTraffic: Array<number> = [ 
    0, 1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 7, 8, 9, 10 
];

export function seekMail(
    checkEffect: number,
    source: World,
    destination: World,
    distance: number,
    maxNavalOrScoutRank: number,
    maxSOC: number,
    shipIsArmed: boolean,
    userDM: number
): number {
    const sourceDMs = getWorldDMs(source);
    const destDMs = getWorldDMs(destination);
    
    const distanceDM: number = Math.min(0, (distance-1)*-1);

    let freightTrafficDM: number = checkEffect + sourceDMs + destDMs 
        + distanceDM + userDM;
    
    let mailDM: number = 0;
    if (freightTrafficDM <= -10) {
        mailDM = -2;
    } else if (-9 <= freightTrafficDM && freightTrafficDM <= -5) {
        mailDM = -1;
    } else if (freightTrafficDM <= -5) {
        mailDM = -1;
    } else if (5 <= freightTrafficDM && freightTrafficDM <= 9) {
        mailDM = 1;
    } else if (freightTrafficDM >= 10) {
        mailDM = 2;
    }

    if (source.UWP.TechLevel <= 5) {
        mailDM -= 4;
    }

    if (destination.UWP.TechLevel <= 5) {
        mailDM -= 4;
    }

    if (shipIsArmed) {
        mailDM += 2;
    }

    const roll = r2d6() + mailDM + maxNavalOrScoutRank + maxSOC;

    if (roll >= 12) {
        return r1d6();
    } else {
        return 0;
    }
}

export function seekFreight(
    checkEffect: number,
    source: World,
    destination: World,
    distance: number,
    cargo: string,
    userDM: number
): Array<number> {
    const sourceDMs = getWorldDMs(source);
    const destDMs = getWorldDMs(destination);
    
    const distanceDM: number = Math.min(0, (distance-1)*-1);

    const freightTrafficDM: number = checkEffect + sourceDMs + destDMs 
        + distanceDM + userDM;

    const roll = r2d6() + freightTrafficDM;

    let val: number = roll <= 1 ? 0 
        : FreightTraffic[Math.max(roll, FreightTraffic.length)-1];
    const numLots: number = dice(val, 6);

    if (cargo === "Major") {
        return [ ...Array(numLots).keys() ].map( i => r1d6()*10);
    } else if (cargo === "Minor") {
        return [ ...Array(numLots).keys() ].map( i => r1d6()*5);
    } else {
        return [ ...Array(numLots).keys() ].map( i => r1d6());
    }
}

function getWorldDMs(world: World): number {
    let popDM: number = 0;
    if (world.UWP.Population <= 1) {
        popDM = -4;
    } else if (world.UWP.Population === 6 || world.UWP.Population === 7) {
        popDM = 2;
    } else if (world.UWP.Population >= 8) {
        popDM = 4;
    }

    let zoneDM: number = 0;
    if (world.Zone === "Amber") {
        zoneDM = -2;
    } else if (world.Zone === "Red") {
        zoneDM = -6;
    }

    let starportDM: number = 0;
    if (world.UWP.Starport === 10 ) {
        starportDM = 2;
    } else if (world.UWP.Starport === 11) {
        starportDM = 1;
    } else if (world.UWP.Starport === 14) {
        starportDM = -1;
    } else if (world.UWP.Starport === 31) {
        starportDM = -3;
    }

    let techLevelDM: number = 0;
    if (world.UWP.TechLevel <= 6) {
        techLevelDM = -1;
    } else if (world.UWP.TechLevel >= 9) {
        techLevelDM = 2;
    }
    
    return popDM + zoneDM + starportDM + techLevelDM;
}

export const exportedForTesting = {
    getWorldDMs
}
  