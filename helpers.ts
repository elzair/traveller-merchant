

export function die(type: number): number {
    if (!Number.isInteger(type)) {
        throw new Error(`die() passed non-integral value: ${type}`);
    } else if (type < 0) {
        throw new Error(`die() input is negative ${type}`);
    }

    return Math.floor(Math.random() * type) + 1;
}

export function dice(num: number, type: number): number {
    if (!Number.isInteger(num)) {
        throw new Error(`dice() passed non-integral num: ${type}`);
    } else if (num < 0) {
        throw new Error(`dice() num is negative ${type}`);
    } if (!Number.isInteger(type)) {
        throw new Error(`dice() passed non-integral type: ${type}`);
    } else if (type < 0) {
        throw new Error(`dice() type is negative ${type}`);
    }

    let sum: number = 0;

    for (let i: number = 0; i < num; i++) {
        sum += die(type);
    }

    return sum;
}

export function r1d6(): number {
    return dice(1, 6);
}

export function r2d6(): number {
    return dice(2, 6);
}

export function check(skill: number): number {
    return r2d6() + skill;
}

export function effect(skill: number, difficulty: number = 8): number {
    return check(skill) - difficulty;
}

export function tetra2num(c: string): number {
    let num: number = c.charCodeAt(0);

    if (48 <= num && num <= 57) {
        num -= 48;
    } else if (65 <= num && num <= 72) {
        num = num - 65 + 10;
    } else if (74 <= num && num <= 78) {
        num = num - 65 + 10 - 1;
    } else if (80 <= num && num <= 90) {
        num = num - 65 + 10 - 2;
    } else if (97 <= num && num <= 104) {
        num = num - 97 + 10;
    } else if (106 <= num && num <= 110) {
        num = num - 97 + 10 - 1;
    } else if (112 <= num && num <= 122) {
        num = num - 97 + 10 - 2;
    } else {
        throw new Error(`tetra2num() passed invalid character: ${c}`);
    }

    return num;
}

export function constrain(num: number, low: number, high: number): number {
    return Math.max(Math.min(num, high), low);
}

export function sample<T>(arr: Array<T>): T {
    return arr[Math.floor(Math.random()*arr.length)];
}

export function zip<T, V>(arr1: Array<T>, arr2: Array<V>): Array<[T,V]> {
    if (arr1.length !== arr2.length) {
        throw new Error("Arrays must have the same length");
    }

    return arr1.map((t, i) => [t, arr2[i]]);
}