import { tetra2num } from './helpers';

export interface UWP {
    Starport: number,
    Size: number,
    Atmosphere: number
    Hydrographics: number
    Population: number,
    Government: number,
    LawLevel: number,
    TechLevel: number
}

/**
* Get trade codes based on UWP data
* @param {UWP} uwp Representation of planet
* @returns Array of strings representing trade codes
*/
export function getTradeCodes(uwp: UWP): Array<string> {

    let codes: Array<string> = [];

    if (uwp.Atmosphere >= 4 && uwp.Atmosphere <= 9 && uwp.Hydrographics >= 4 
        && uwp.Hydrographics <= 8 && uwp.Population >= 5 
        && uwp.Population <= 7) {
        codes.push('Ag'); // Agricultural
    }

    if (uwp.Size === 0 && uwp.Atmosphere === 0 && uwp.Hydrographics=== 0) {
        codes.push('As'); // Asteroid
    }

    if (uwp.Population === 0 && uwp.Government === 0 && uwp.LawLevel === 0) {
        codes.push('Ba'); // Barren
    }

    if (uwp.Atmosphere >= 2 && uwp.Hydrographics === 0) {
        codes.push('De'); // Desert
    }

    if (uwp.Atmosphere >= 10 && uwp.Hydrographics >= 1) {
        codes.push('Fl'); // Fluid Oceans
    }

    if ((uwp.Atmosphere === 5 || uwp.Atmosphere === 6 
        || uwp.Atmosphere === 8) && uwp.Hydrographics >= 4 
        && uwp.Hydrographics <= 9 && uwp.Population >= 4 
        && uwp.Population <= 8) {
        codes.push('Ga'); // Garden
    }

    if (uwp.Population >= 9) {
        codes.push('Hi'); // High Population
    }

    if (uwp.TechLevel >= 12) {
        codes.push('Ht'); // High Technology
    }

    if (uwp.Atmosphere >= 0 && uwp.Atmosphere <= 1 
        && uwp.Hydrographics >= 1) {
        codes.push('Ic'); // Ice-Capped
    }

    if (((uwp.Atmosphere >= 0 && uwp.Atmosphere <= 2) 
        || uwp.Atmosphere === 4 || uwp.Atmosphere === 7 
        || uwp.Atmosphere === 9) && uwp.Population >= 9) {
        codes.push('In'); // Industrial
    }

    if (uwp.Population >= 0 && uwp.Population <= 3) {
        codes.push('Lo'); // Low Population
    }

    if (uwp.TechLevel <= 5) {
        codes.push('Lt'); // Low Technology
    }

    if (uwp.Atmosphere >= 0 && uwp.Atmosphere <= 3 
        && uwp.Hydrographics >= 0 && uwp.Hydrographics <= 3 
        && uwp.Population >= 6) {
        codes.push('Na'); // Non-Agricultural
    }

    if (uwp.Population >= 4 && uwp.Population <= 6) {
        codes.push('Ni'); // Non-Industrial
    }

    if (uwp.Atmosphere >= 2 && uwp.Atmosphere <= 5 
        && uwp.Hydrographics >= 0 && uwp.Hydrographics <= 3) {
        codes.push('Po'); // Poor
    }

    if ((uwp.Atmosphere === 6 || uwp.Atmosphere === 8) 
        && uwp.Population >= 6 && uwp.Population <= 8) {
        codes.push('Ri'); // Rich
    }

    if (uwp.Hydrographics === 10) {
        codes.push('Wa'); // Water World
    }

    if (uwp.Atmosphere === 0) {
        codes.push('Va'); // Vacuum
    }

    return codes;
}

export function getUWP(uwpStr: string): UWP {
    const re = /^(?<St>[A-FX0-9])(?<Si>[A0-9])(?<At>[A-F0-9])(?<Hy>[A0-9])(?<Pop>[A-C0-9])(?<Gov>[A-F0-9])(?<LL>[A-F0-9])(?:\-)?(?<TL>[A-F0-9])$/;
    const matches = uwpStr.match(re);

    if (matches === null) {
        throw new Error(`getUWP() cannot match "${uwpStr}"`);
    }

    const uwp = {
        Starport: tetra2num(matches![1]),
        Size: tetra2num(matches![2]),
        Atmosphere: tetra2num(matches![3]),
        Hydrographics: tetra2num(matches![4]),
        Population: tetra2num(matches![5]),
        Government: tetra2num(matches![6]),
        LawLevel: tetra2num(matches![7]),
        TechLevel: tetra2num(matches![8])
    };

    return uwp;
}

export interface World {
    UWP: UWP,
    Zone: string,
    TradeCodes: Array<string>
}

export function getWorld(uwpStr: string, zone: string = "None") {
    const uwp = getUWP(uwpStr);

    const tradeCodes = getTradeCodes(uwp);

    if (zone === "Red") {
        tradeCodes.push("Rz");
    } else if (zone === "Amber") {
        tradeCodes.push("Az");
    }

    return {
        UWP: uwp,
        Zone: zone,
        TradeCodes: tradeCodes
    };
}