import { World } from "./worldinfo";
import { TradeGood, TradeGoods } from "./shared";
import { constrain, dice, sample, zip } from "./helpers";

const ModifiedPurchasePrice: Array<number> = [ 
    3, 2.5, 2, 1.75, 1.5, 1.35, 1.25, 1.2, 1.15, 1.1, 1.05, 1, 
    .95, .9, .85, .8, .75, .7, .65, .6, .55, .5, .45, .4, .35, 
    .3, .25, .2, .15 
];

const ModifiedSellPrice: Array<number> = [ 
    .1, .2, .3, .4, .5, .55, .6, .65, .7, .75, .8, .85, .9, 1, 
    1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.4, 1.5, 1.6, 1.75, 2, 
    2.5, 3, 4 
];

export function seekGoods(
    source: World, 
    travellerMaxBrokerSkill: number, 
    supplierBrokerSkill: number = 2, 
    blackMarketSupplier: boolean = false 
) {
    const available = getAvailableGoods(source, blackMarketSupplier);
    const goods = [...available.keys()].map(g1 => 
        TradeGoods.find(g2 => g1 === g2.GoodType)!);
    const multiples = [...available.values()];

    // Add -3 to availability on low population worlds
    const quantityDM = source.UWP.Population <= 3 ? -3 : 0;

    const mapFunc = ([good, multiple]: [TradeGood, number]) => {
        const quantity = constrain(
            dice(good.Tons[0], 6) * good.Tons[1] * multiple + quantityDM,
            -3, 
            25
        );

        // Determine price of good
        const priceRoll = constrain(
            dice(3, 6) + travellerMaxBrokerSkill 
                + getBuySellDMs(source, good) - supplierBrokerSkill,
            -3,
            25
        );
        const price = good.BasePrice * ModifiedPurchasePrice[priceRoll+3];
    }

    return zip(goods, multiples).map(mapFunc);
}

function getAvailableGoods(
    source: World, 
    blackMarketSupplier: boolean = false
): Map<string, number> {
    const filterFunc = (tradeGood: TradeGood) => {
        if (tradeGood.Availability === "All") {
            return true;
        }

        if (Array.isArray(tradeGood)) {
            for (const tradeCode of source.TradeCodes) {
                if (tradeGood.Availability.includes(tradeCode)) {
                    if (!tradeGood.Illegal || blackMarketSupplier) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    // Get available goods and return a `Map` of GoodTypes to the 
    // quantity available
    const available1 = TradeGoods.filter(filterFunc);
    const available: Map<string, number> = new Map();

    for (const av of available1) {
        available.set(av.GoodType, 1);
    }

    // Add a number of random goods up to the World's Population Code
    const numRandomGoods: number = source.UWP.Population;

    for (let i = 0; i<numRandomGoods; i++) {
        const good = sample(TradeGoods);
        available.set(
            good.GoodType, 
            available.has(good.GoodType) 
                ? available.get(good.GoodType)!+1 : 1
        );
    }

    return available;
}

function getBuySellDMs(
    source: World, 
    tradeGood: TradeGood, 
    selling: boolean = false
): number {
    const filterFunc = ([tradeCode, dm]: [string, number]) => 
        source.TradeCodes.includes(tradeCode);

    const buyDMs0 = new Map(Object.entries(tradeGood.PurchaseDM));
    const buyDMs = new Map([...buyDMs0].filter(filterFunc));
    const maxBuyDM = Math.max(...buyDMs.values());

    const sellDMs0 = new Map(Object.entries(tradeGood.SaleDM));
    const sellDMs = new Map([...sellDMs0].filter(filterFunc));
    const maxSellDM = Math.max(...sellDMs.values());

    return selling ? (maxSellDM - maxBuyDM) : (maxBuyDM - maxSellDM);
}